use crate::{parser_models::TransactionHeader, FilterDecision, TransactionFilter};

pub struct DummyTransactionFilter;

impl TransactionFilter for DummyTransactionFilter {
    fn filter_transaction_header<'a>(&self, _header: &TransactionHeader<'a>) -> FilterDecision {
        FilterDecision::Select
    }
}
