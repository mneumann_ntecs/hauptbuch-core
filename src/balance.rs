use crate::{AccountRepository, CommodityRepository};

pub struct Balance {
    pub accounts: AccountRepository,
    pub commodities: CommodityRepository,
}
