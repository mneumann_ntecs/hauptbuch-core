use hauptbuch_parser::{
    comment::Comment, line::Line, posting::Posting, transaction_header::TransactionHeader,
};

pub trait LineVisitor {
    type Error;
    type Value;

    fn accept(&mut self, line: Line) -> Result<Self::Value, Self::Error> {
        match line {
            Line::Posting(posting) => self.visit_posting(posting),
            Line::Empty => self.visit_empty_line(),
            Line::FileComment(comment) => self.visit_file_comment(comment),
            Line::TransactionHeader(txn_header) => self.visit_transaction_header(txn_header),
            Line::TransactionComment(txn_comment) => self.visit_transaction_comment(txn_comment),
        }
    }

    fn visit_transaction_header<'a>(
        &mut self,
        header: TransactionHeader<'a>,
    ) -> Result<Self::Value, Self::Error>;

    fn visit_transaction_comment<'a>(
        &mut self,
        comment: Comment<'a>,
    ) -> Result<Self::Value, Self::Error>;

    fn visit_posting<'a>(&mut self, posting: Posting<'a>) -> Result<Self::Value, Self::Error>;

    fn visit_file_comment(&mut self, comment: &str) -> Result<Self::Value, Self::Error>;

    // This can be used to track line numbers.
    fn visit_empty_line(&mut self) -> Result<Self::Value, Self::Error>;

    /// Called when entering a new file.
    ///
    /// This can be used to record which file we are currently in.
    fn visit_enter_new_file(&mut self, filename: &str) -> Result<Self::Value, Self::Error>;

    /// This gets called when we encounter the end of the file.
    fn visit_end_of_file(&mut self) -> Result<Self::Value, Self::Error>;
}
