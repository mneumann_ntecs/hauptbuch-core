use crate::{parser_models::TransactionHeader, FilterDecision};

pub trait TransactionFilter {
    fn filter_transaction_header<'a>(&self, header: &TransactionHeader<'a>) -> FilterDecision;
}
