#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum FilterDecision {
    Select,
    Reject,
}
