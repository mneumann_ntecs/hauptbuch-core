use crate::Account;
use std::collections::HashMap;

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
pub struct AccountId(usize);

#[derive(Debug, Copy, Clone)]
pub enum AccountStrictMode {
    Enabled,
    Disabled,
}

/// Maintains a collection of accounts and allows to look them up by name in order to obtain an
/// AccountId.
///
/// We convert account names to account ids for performance reasons and to avoid referencing hell.
#[derive(Debug)]
pub struct AccountRepository {
    account_map: HashMap<String, AccountId>,
    accounts: Vec<Account>,
    strict_mode: AccountStrictMode,
}

impl AccountRepository {
    pub fn new(initial_accounts: Vec<Account>, strict_mode: AccountStrictMode) -> Result<Self, ()> {
        let mut account_map = HashMap::new();

        for (account_id, account) in initial_accounts.iter().enumerate() {
            if let Some(_existing) =
                account_map.insert(account.fullname.clone(), AccountId(account_id))
            {
                // duplicate account name
                return Err(());
            }
        }

        Ok(Self {
            accounts: initial_accounts,
            account_map,
            strict_mode,
        })
    }

    pub fn account_iter(&self) -> impl Iterator<Item = &Account> {
        self.accounts.iter()
    }

    pub fn accounts_iter_with_id(&self) -> impl Iterator<Item = (&Account, AccountId)> {
        self.accounts
            .iter()
            .enumerate()
            .map(|(idx, account)| (account, AccountId(idx)))
    }

    pub fn get_or_add_id(&mut self, account_name: &str) -> Result<AccountId, ()> {
        match self.account_map.get(account_name).cloned() {
            // return existing account
            Some(account_id) => Ok(account_id),
            None => match self.strict_mode {
                AccountStrictMode::Enabled => Err(()),
                AccountStrictMode::Disabled => {
                    let account = Account::new(account_name.into());
                    let idx = AccountId(self.accounts.len());
                    assert!(self.account_map.insert(account_name.into(), idx).is_none());
                    self.accounts.push(account);
                    Ok(idx)
                }
            },
        }
    }

    pub fn get_by_id_mut(&mut self, account_id: AccountId) -> Option<&mut Account> {
        self.accounts.get_mut(account_id.0)
    }

    pub fn lookup_id_by_fullname(&self, account_name: &str) -> Option<AccountId> {
        self.account_map.get(account_name).cloned()
    }

    pub fn lookup_by_fullname(&self, account_name: &str) -> Option<&Account> {
        self.account_map
            .get(account_name)
            .and_then(|&AccountId(idx)| self.accounts.get(idx))
    }

    pub fn lookup_by_fullname_mut(&mut self, account_name: &str) -> Option<&mut Account> {
        match self.account_map.get(account_name) {
            Some(&AccountId(idx)) => self.accounts.get_mut(idx),
            None => None,
        }
    }
}

#[test]
fn it_should_correctly_lookup_account_by_fullname() {
    let mut repo = AccountRepository::new(vec![
        Account::new("Expenses:IceCream".into()),
        Account::new("Expenses:IceCream2".into()),
    ])
    .unwrap();

    assert_eq!(
        Some("Expenses:IceCream"),
        repo.lookup_by_fullname("Expenses:IceCream")
            .map(|acct| acct.fullname.as_str())
    );
    assert_eq!(
        Some("Expenses:IceCream2"),
        repo.lookup_by_fullname("Expenses:IceCream2")
            .map(|acct| acct.fullname.as_str())
    );
    assert_eq!(
        None,
        repo.lookup_by_fullname("Expenses:IceCream3")
            .map(|acct| acct.fullname.as_str())
    );
}
