use crate::{
    decimal::Decimal, parser_models, CommodityId, CommodityRepository, ProcessingError, Value,
};
use std::str::FromStr;

pub fn decimal(s: &str) -> Result<Decimal, ProcessingError> {
    Decimal::from_str(s).map_err(|_| ProcessingError::DecimalConversionError)
}

pub fn commodity_id_from_opt_str(
    commodities: &mut CommodityRepository,
    opt_str: Option<&str>,
) -> Result<CommodityId, ProcessingError> {
    match opt_str {
        None => commodities
            .get_default_commodity_id()
            .ok_or(ProcessingError::NoDefaultCommoditySpecified),
        Some(s) => commodities
            .get_or_add(s)
            .map_err(|_| ProcessingError::StrictCommodityRegistry),
    }
}

fn round_value(
    commodities: &mut CommodityRepository,
    value: Value,
) -> Result<Value, ProcessingError> {
    let Value {
        commodity_id,
        amount,
    } = value;
    let commodity = commodities
        .lookup_by_id(commodity_id)
        .ok_or(ProcessingError::FatalCommodityLookupFailure)?;
    let amount = commodity.round_value(amount);
    Ok(Value {
        amount,
        commodity_id,
    })
}

fn position_to_value_and_round(
    commodities: &mut CommodityRepository,
    position: &parser_models::Position,
) -> Result<Value, ProcessingError> {
    let value = position_to_value_no_round(commodities, position)?;
    round_value(commodities, value)
}

fn position_to_value_no_round(
    commodities: &mut CommodityRepository,
    position: &parser_models::Position,
) -> Result<Value, ProcessingError> {
    let commodity_id = commodity_id_from_opt_str(commodities, position.commodity)?;
    match position.amount {
        parser_models::Amount::Numeric(amount) => Ok(Value {
            amount: decimal(amount)?,
            commodity_id,
        }),
        parser_models::Amount::Duration {
            hours,
            minutes,
            seconds,
        } => Ok(Value {
            amount: Decimal::new(
                (hours as i64 * 3600) + (minutes as i64 * 60) + (seconds as i64),
                1,
            )
            .checked_div(Decimal::new(3600, 1))
            .unwrap(),
            commodity_id,
        }),

        // XXX
        _ => Err(ProcessingError::DecimalConversionError),
    }
}

// Calculate the closing value of a posting.
//
// All postings of a transaction must be convertable into the same
// commodity. We use this to check if a transaction balances, i.e. it's
// balance is 0. The amount we transfer to an account might be in a
// different commodity.
//
// Convert the posting `value` into the commodity as specified
// by the `closing_position`. For example:
//
//     "1 BTC @ 1000 EUR" would use 1000 EUR as closing value.
//     "0.5 BTC @ 1000 EUR" would use 500 EUR as closing value.
//     "0.5 BTC = 1000 EUR" would use 1000 EUR as closing value.
pub fn calc_value_and_closing_value(
    commodities: &mut CommodityRepository,
    posting_value: &parser_models::PostingValue,
) -> Result<(Value, Value), ProcessingError> {
    // We round the posting value
    let value = position_to_value_and_round(commodities, &posting_value.value_position)?;

    let closing_value_through_unit_price_not_rounded =
        if let Some(closing_unit_price) = &posting_value.closing_unit_price {
            // we do not round the unit price!
            let unit_price = position_to_value_no_round(commodities, closing_unit_price)?;

            if value.has_same_commodity(&unit_price) {
                // For example: "1 EUR @ 100 EUR" makes no sense
                return Err(ProcessingError::ValuePositionWithSameCommodityAsMainPositionDetected);
            }

            Some(Value {
                amount: value.amount * unit_price.amount,
                commodity_id: unit_price.commodity_id,
            })
        } else {
            None
        };

    let closing_value_through_total_price_not_rounded =
        if let Some(closing_total_price) = &posting_value.closing_total_price {
            Some(position_to_value_no_round(
                commodities,
                closing_total_price,
            )?)
        } else {
            None
        };

    // Check that if we have a unit price AND a total price, both results have to match!
    if let (Some(through_unit), Some(through_total)) = (
        &closing_value_through_unit_price_not_rounded,
        &closing_value_through_total_price_not_rounded,
    ) {
        if !through_unit.has_same_commodity(&through_total) {
            return Err(ProcessingError::UnitPriceAndTotalPriceMustHaveSameCommodity);
        }

        if through_unit.amount != through_total.amount {
            return Err(ProcessingError::UnitPriceAndTotalPriceInvalidCalculation);
        }
    }

    let closing_value = match closing_value_through_unit_price_not_rounded
        .or(closing_value_through_total_price_not_rounded)
    {
        Some(c) => round_value(commodities, c)?,
        None => value,
    };

    Ok((value, closing_value))
}
