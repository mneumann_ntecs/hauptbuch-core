use crate::{OptValue, Value};

#[derive(Debug)]
pub struct Account {
    /// The full name of the account, e.g. "Expenses:IceCream".
    pub fullname: String,

    /// The current value stored in the account. An account can hold only values in one commodity.
    /// As we initially do not know the commodity of the account, we give it the initial value None.
    pub value: OptValue,
}

impl Account {
    pub fn new(fullname: String) -> Self {
        Self::new_with_value(fullname, OptValue::new())
    }

    pub fn new_with_value(fullname: String, value: OptValue) -> Self {
        Self { fullname, value }
    }

    /// Transfers `value` to the account. `Value` has to be of the same commodity.
    pub fn transfer<'a>(&mut self, value: Value) -> Result<(), ()> {
        self.value.transfer(value)
    }
}
