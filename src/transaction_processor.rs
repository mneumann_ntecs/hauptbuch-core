use crate::{
    parser_models::{self, *},
    utils::calc_value_and_closing_value,
    AccountId, AccountRepository, Balance, CommodityId, CommodityRepository, FilterDecision,
    LineVisitor, OptValue, ProcessingError, TransactionFilter, Value,
};
use chrono::offset::Utc;

pub trait TransactionInfo: Sized {
    type Storage: Sized;
    fn new_from_transaction_header(
        &self,
        header: TransactionHeader,
    ) -> Result<Self::Storage, ProcessingError>;
}

impl TransactionInfo for () {
    type Storage = ();
    fn new_from_transaction_header(
        &self,
        _header: TransactionHeader,
    ) -> Result<Self::Storage, ProcessingError> {
        Ok(())
    }
}

pub struct AdditionalTransactionInfo {
    pub local_timezone: parser_models::Timezone,
}

pub struct AdditionalTransactionInfoStorage {
    pub display_datetime: DateTime,
    pub datetime: chrono::DateTime<Utc>,
    pub status_mark: StatusMark,
    pub code: Option<String>,
    pub description: String,
    pub attributes: Vec<String>,
}

// Assumes Date an DateTime to be in `local_timezone`.
pub fn to_datetime(datetime: &DateTime, local_timezone: Timezone) -> chrono::DateTime<Utc> {
    use chrono::{Datelike, FixedOffset, TimeZone, Timelike};
    let (dt, timezone) = match *datetime {
        DateTime::Date(date) => (date.and_hms(0, 0, 0), local_timezone),
        DateTime::DateTime(datetime) => (datetime, local_timezone),
        DateTime::DateTimeTz(datetime, timezone) => (datetime, timezone),
    };
    match timezone {
        Timezone::Utc => chrono::DateTime::<Utc>::from_utc(dt, Utc),
        Timezone::FixedOffset(mins) => chrono::DateTime::from(
            FixedOffset::east(mins as i32 * 60)
                .ymd(dt.year(), dt.month(), dt.day())
                .and_hms(dt.hour(), dt.minute(), dt.second()),
        ),
    }
}

impl TransactionInfo for AdditionalTransactionInfo {
    type Storage = AdditionalTransactionInfoStorage;
    fn new_from_transaction_header(
        &self,
        header: TransactionHeader,
    ) -> Result<Self::Storage, ProcessingError> {
        let storage = AdditionalTransactionInfoStorage {
            display_datetime: header.datetime,
            datetime: to_datetime(&header.datetime, self.local_timezone),
            status_mark: header.status_mark,
            code: header.code.map(|s| s.into()),
            description: header.description.into(),
            attributes: vec![],
        };

        Ok(storage)
    }
}

pub trait CommitTransaction<I: TransactionInfo> {
    fn commit_transaction(
        &mut self,
        accounts: &mut AccountRepository,
        txn: ResolvedTransactionRecord<I>,
    ) -> Result<(), ProcessingError>;
}

pub struct TransactionCommitter;

impl<I: TransactionInfo> CommitTransaction<I> for TransactionCommitter {
    fn commit_transaction(
        &mut self,
        accounts: &mut AccountRepository,
        txn: ResolvedTransactionRecord<I>,
    ) -> Result<(), ProcessingError> {
        let mut check_balance = Value::zero(txn.balance_commodity_id);

        // Now commit all postings to the accounts. If we fail here, we are left in an inconsistent
        // state.

        // XXX: Implement iterator for ResolvedTransactionRecord
        for posting_record in txn.postings.iter() {
            let PostingRecordValue {
                value,
                closing_value,
                posting_type,
            } = posting_record.value.expect("FATAL");

            let account = accounts
                .get_by_id_mut(posting_record.account_id)
                .ok_or(ProcessingError::FatalAccountLookupFailure)?;

            // Transfer amount to account
            if let Err(_) = account.transfer(value) {
                return Err(ProcessingError::CommodityMismatch);
            }

            // Record check balance for regular postings. Basically double-checking
            // that the transaction balances to zero. This shouldn't be neccessary!
            if let PostingType::RegularPosting = posting_type {
                check_balance
                    .transfer(closing_value)
                    .expect("FATAL: Invalid check balance commodity");
            }
        }

        if !check_balance.is_zero() {
            return Err(ProcessingError::TransactionNotBalanced);
        }

        Ok(())
    }
}

pub struct TransactionRecorder<I: TransactionInfo> {
    pub transactions: Vec<ResolvedTransactionRecord<I>>,
}

impl<I: TransactionInfo> TransactionRecorder<I> {
    pub fn new() -> Self {
        Self {
            transactions: vec![],
        }
    }
}

impl<I: TransactionInfo> CommitTransaction<I> for TransactionRecorder<I> {
    fn commit_transaction(
        &mut self,
        _accounts: &mut AccountRepository,
        txn: ResolvedTransactionRecord<I>,
    ) -> Result<(), ProcessingError> {
        self.transactions.push(txn);
        Ok(())
    }
}

pub struct TransactionProcessor<T: TransactionFilter, C: CommitTransaction<I>, I: TransactionInfo> {
    current_balance: Balance,
    open_transaction: Option<TransactionRecord<I>>,
    transaction_filter: Box<T>,
    commit_transaction: Box<C>,
    transaction_info: I,
}

#[derive(Debug)]
pub struct PostingRecord {
    pub account_id: AccountId,
    pub value: Option<PostingRecordValue>,
}

#[derive(Debug, Copy, Clone)]
pub struct PostingRecordValue {
    pub posting_type: PostingType,
    pub value: Value,
    pub closing_value: Value,
}

/// Keeps state while we going through the postings of a transaction.
#[derive(Debug)]
struct TransactionRecord<I: TransactionInfo> {
    /// If a filter decides to ignore the transaction, it sets this flag.
    ignore_transaction: bool,

    /// The postings in the transaction.
    postings: Vec<PostingRecord>,

    /// Here, more information can be stored about the transaction
    info: I::Storage,
}

/// A TransactionRecord that is validated and a potential valueless position
/// has been resolved.
#[derive(Debug)]
pub struct ResolvedTransactionRecord<I: TransactionInfo> {
    pub postings: Vec<PostingRecord>,
    balance_commodity_id: CommodityId,
    pub info: I::Storage,
}

impl<I: TransactionInfo> TransactionRecord<I> {
    pub fn validate_and_resolve(mut self) -> Result<ResolvedTransactionRecord<I>, ProcessingError> {
        let (open_balance, valueless_posting_index_opt) = self.validate()?;
        if let Some(idx) = valueless_posting_index_opt {
            self.resolve(open_balance, idx);
        }
        Ok(ResolvedTransactionRecord {
            postings: self.postings,
            balance_commodity_id: open_balance.commodity_id,
            info: self.info,
        })
    }

    fn resolve(&mut self, open_balance: Value, valueless_posting_index: usize) {
        // Update valueless posting in place if there is one
        let value = open_balance.negate();
        let closing_value = value;
        let posting_type = PostingType::RegularPosting;

        assert!(self.postings[valueless_posting_index].value.is_none());
        self.postings[valueless_posting_index].value = Some(PostingRecordValue {
            value,
            closing_value,
            posting_type,
        });
    }

    fn validate(&self) -> Result<(Value, Option<usize>), ProcessingError> {
        if self.postings.is_empty() {
            return Err(ProcessingError::TransactionEmpty);
        }

        // Calculate open balance
        let mut open_balance = OptValue::new();

        // Record the index of the valueless posting
        let mut valueless_posting_index: Option<usize> = None;

        for (idx, posting_record) in self.postings.iter().enumerate() {
            match &posting_record.value {
                Some(PostingRecordValue {
                    posting_type,
                    closing_value,
                    ..
                }) => {
                    match posting_type {
                        PostingType::RegularPosting => {
                            if let Err(_) = open_balance.transfer(*closing_value) {
                                return Err(ProcessingError::MultiValuedBalanceNotAllowed);
                            }
                        }
                        PostingType::VirtualPosting => {
                            // Ignore Virtual postings for open balance calculation
                        }
                    }
                }
                None => {
                    if valueless_posting_index.is_none() {
                        valueless_posting_index = Some(idx);
                    } else {
                        return Err(ProcessingError::AtMostOneValuelessPostingAllowed);
                    }
                }
            }
        }

        // There must be an open balance, as we have at least 2 postings, and only one of them can
        // be value-less.
        let open_balance = open_balance
            .value
            .ok_or(ProcessingError::FatalOpenBalanceHasNoValue)?;

        // If we don't have a posting without value, the open_balance must be zero
        if valueless_posting_index.is_none() && !open_balance.is_zero() {
            return Err(ProcessingError::TransactionNotBalanced);
        }

        Ok((open_balance, valueless_posting_index))
    }
}

impl<T: TransactionFilter, C: CommitTransaction<I>, I: TransactionInfo>
    TransactionProcessor<T, C, I>
{
    pub fn new(
        initial_balance: Balance,
        transaction_filter: Box<T>,
        commit_transaction: Box<C>,
        transaction_info: I,
    ) -> Self {
        Self {
            current_balance: initial_balance,
            open_transaction: None,
            transaction_filter,
            commit_transaction,
            transaction_info,
        }
    }

    pub fn unwrap(self) -> (AccountRepository, CommodityRepository, Box<C>) {
        assert!(self.open_transaction.is_none());
        let TransactionProcessor {
            current_balance:
                Balance {
                    accounts,
                    commodities,
                },
            commit_transaction,
            ..
        } = self;

        (accounts, commodities, commit_transaction)
    }
    pub fn get_balance(self) -> Balance {
        self.current_balance
    }

    fn end_transaction(&mut self) -> Result<(), ProcessingError> {
        if let Some(open) = self.open_transaction.take() {
            self.finalize_transaction(open)?;
        }
        debug_assert!(self.open_transaction.is_none());
        Ok(())
    }

    fn finalize_transaction(&mut self, txn: TransactionRecord<I>) -> Result<(), ProcessingError> {
        if txn.ignore_transaction {
            return Ok(());
        }

        let resolved_txn = txn.validate_and_resolve()?;
        let _ = self
            .commit_transaction
            .commit_transaction(&mut self.current_balance.accounts, resolved_txn)?;
        Ok(())
    }
}

impl<T: TransactionFilter, C: CommitTransaction<I>, I: TransactionInfo> LineVisitor
    for TransactionProcessor<T, C, I>
{
    type Error = ProcessingError;
    type Value = ();

    fn visit_transaction_header(&mut self, header: TransactionHeader) -> Result<(), Self::Error> {
        self.end_transaction()?;
        assert!(self.open_transaction.is_none());

        let ignore_transaction =
            self.transaction_filter.filter_transaction_header(&header) == FilterDecision::Reject;

        self.open_transaction = Some(TransactionRecord {
            postings: Vec::new(),
            ignore_transaction,
            info: self.transaction_info.new_from_transaction_header(header)?,
        });

        Ok(())
    }

    fn visit_transaction_comment(&mut self, comment: Comment) -> Result<(), Self::Error> {
        match &self.open_transaction {
            None => Err(ProcessingError::TransactionCommentOutsideTransaction),
            Some(_txn) => {
                if let Comment::Attribute(_) = comment {
                    // XXX: parse/attach meta data
                    Ok(()) //XXX: Err(ProcessingError::from("Transaction comment unimplemented"))
                } else {
                    Ok(())
                }
            }
        }
    }

    fn visit_posting(&mut self, posting: Posting) -> Result<(), Self::Error> {
        match &mut self.open_transaction {
            None => Err(ProcessingError::PostingOutsideTransaction),
            Some(txn) => {
                if let Some(Comment::Attribute(_)) = posting.comment {
                    return Err(ProcessingError::PostingCommentsUnimplemented);
                }

                // Lookup account id
                let account_id = self
                    .current_balance
                    .accounts
                    .get_or_add_id(posting.account_name)
                    .map_err(|_| ProcessingError::StrictAccountRegistry)?;

                let posting_record = match posting.posting_value {
                    None => match posting.posting_type {
                        PostingType::RegularPosting => PostingRecord {
                            account_id,
                            value: None,
                        },
                        _ => Err(ProcessingError::ValuelessPostingOfNonRegularType)?,
                    },
                    Some(posting_value) => {
                        let (value, closing_value) = calc_value_and_closing_value(
                            &mut self.current_balance.commodities,
                            &posting_value,
                        )?;

                        PostingRecord {
                            account_id,
                            value: Some(PostingRecordValue {
                                posting_type: posting.posting_type,
                                closing_value,
                                value,
                            }),
                        }
                    }
                };

                txn.postings.push(posting_record);

                Ok(())
            }
        }
    }

    fn visit_end_of_file(&mut self) -> Result<(), Self::Error> {
        self.end_transaction()?;
        Ok(())
    }

    fn visit_enter_new_file(&mut self, _filename: &str) -> Result<(), Self::Error> {
        self.end_transaction()?;
        Ok(())
    }

    fn visit_file_comment(&mut self, _comment: &str) -> Result<(), Self::Error> {
        Ok(())
    }

    fn visit_empty_line(&mut self) -> Result<(), Self::Error> {
        Ok(())
    }
}
