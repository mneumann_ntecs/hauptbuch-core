use crate::line_visitor::LineVisitor;
use hauptbuch_parser::line::line as parse_line;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::Path;

#[derive(Debug)]
pub enum LineProcessorError<E> {
    ParseError { line_no: usize, error: String },
    VisitorError { line_no: usize, error: E },
    IoError(std::io::Error),
}

pub struct LineProcessor;

impl LineProcessor {
    pub fn run_file<V>(
        &self,
        path: &Path,
        line_visitor: &mut V,
    ) -> Result<usize, LineProcessorError<V::Error>>
    where
        V: LineVisitor,
    {
        match File::open(path) {
            Ok(file) => {
                let mut buf_reader = BufReader::new(file);
                self.run::<_, _>(&mut buf_reader, line_visitor)
            }
            Err(err) => Err(LineProcessorError::IoError(err)),
        }
    }

    pub fn run<R, V>(
        &self,
        mut rd: R,
        line_visitor: &mut V,
    ) -> Result<usize, LineProcessorError<V::Error>>
    where
        R: BufRead,
        V: LineVisitor,
    {
        let mut line_no = 0; // we enumerate lines starting at 0
        let mut line_buffer = String::new();

        loop {
            line_buffer.clear();

            let _ = match rd.read_line(&mut line_buffer) {
                Ok(n) => {
                    if n == 0 {
                        break;
                    }

                    match parse_line(&line_buffer) {
                        Ok(("", line)) => line_visitor
                            .accept(line)
                            .map_err(|error| LineProcessorError::VisitorError { line_no, error }),
                        Ok((_, _)) => {
                            // line() would not have parsed all data. This is impossible the way line()
                            // is implemented as it uses `exact!`. If we would hit this branch, it
                            // would be a program error that we have to fix!
                            unreachable!();
                        }
                        Err(error) => Err(LineProcessorError::ParseError {
                            line_no,
                            error: format!("{:?}", error),
                        }),
                    }
                }
                Err(io_err) => Err(LineProcessorError::IoError(io_err)),
            }?;
            line_no += 1;
        }

        // eof
        let _ = line_visitor
            .visit_end_of_file()
            .map_err(|error| LineProcessorError::VisitorError { line_no, error })?;

        return Ok(line_no);
    }
}
