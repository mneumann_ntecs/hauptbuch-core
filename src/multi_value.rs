use crate::{CommodityId, OptValue, Value};
use rust_decimal::Decimal;

/// A MultiValue can hold values of different commodities.
#[derive(Debug)]
pub struct MultiValue {
    values: Vec<Value>,
}

impl MultiValue {
    pub fn new() -> Self {
        Self { values: vec![] }
    }

    pub fn values(&self) -> &[Value] {
        &self.values[..]
    }

    pub fn is_zero(&self) -> bool {
        self.values().iter().all(|value| value.is_zero())
    }

    pub fn add_opt_value(&mut self, other_opt_value: &OptValue) {
        if let Some(value) = other_opt_value.value {
            self.add_value(value);
        }
    }

    pub fn add_multi_value(&mut self, other_multi_value: &MultiValue) {
        for other_value in other_multi_value.values.iter() {
            self.add_value(*other_value);
        }
    }

    pub fn add_value(&mut self, other_value: Value) {
        match self
            .values
            .iter_mut()
            .find(|value| value.has_same_commodity(&other_value))
        {
            Some(value) => {
                assert!(value.has_same_commodity(&other_value));
                value.amount += other_value.amount;
            }
            None => {
                self.values.push(other_value);
            }
        }
    }

    pub fn amount_in_commodity(&self, commodity_id: CommodityId) -> Option<Decimal> {
        self.values
            .iter()
            .find(|value| value.commodity_id == commodity_id)
            .map(|value| value.amount)
    }
}
