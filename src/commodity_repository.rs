use crate::decimal::{Decimal, RoundingStrategy};
use crate::CommodityId;

#[derive(Debug, Copy, Clone)]
pub enum Rounding {
    None,
    BankersDp(u8),
}

#[derive(Debug)]
pub struct Commodity {
    name: String,
    rounding: Rounding,
}

impl Commodity {
    pub fn new(name: String, rounding: Rounding) -> Self {
        Self { name, rounding }
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn round_value(&self, d: Decimal) -> Decimal {
        match self.rounding {
            Rounding::None => d,
            Rounding::BankersDp(prec) => {
                d.round_dp_with_strategy(prec as u32, RoundingStrategy::BankersRounding)
            }
        }
    }
}

#[derive(Debug)]
pub enum CommodityStrictMode {
    /// If strict mode is enabled, the CommodityRegistry does not allow any new commodities to be
    /// added. If we try to lookup a commodity that does not exist, this is an error.
    Enabled,

    /// If strict mode is disabled, commodities are added when they are seen for the first time.
    Disabled { default_rounding: Rounding },
}

/// Maintains a collection of commodities and allows to look them up by
/// name in order to obtain a CommodityId.
///
/// We convert commodity names to commodity ids for performance reasons.
/// As we usually do not have many commodities, we use a linear list.
#[derive(Debug)]
pub struct CommodityRepository {
    // We assume that the number of commodities is relatively small, so that a linear search
    // is fast enough.
    commodities: Vec<Commodity>,
    strict_mode: CommodityStrictMode,
    default_commodity_id: Option<CommodityId>,
}

fn has_duplicates(commodities: &[Commodity]) -> bool {
    for commodity in commodities {
        let cnt = commodities
            .iter()
            .filter(|c| c.name == commodity.name)
            .count();
        assert!(cnt >= 1);
        if cnt > 1 {
            return true;
        }
    }
    return false;
}

impl CommodityRepository {
    pub fn new(
        initial_commodities: Vec<Commodity>,
        strict_mode: CommodityStrictMode,
        default_commodity: Option<&str>,
    ) -> Result<Self, ()> {
        if has_duplicates(&initial_commodities[..]) {
            return Err(());
        }

        let default_commodity_id = match default_commodity {
            Some(default_name) => Some(
                initial_commodities
                    .iter()
                    .position(|c| c.name == default_name)
                    .map(|i| CommodityId::new(i))
                    .ok_or(())?,
            ),
            None => None,
        };

        Ok(Self {
            commodities: initial_commodities,
            strict_mode,
            default_commodity_id,
        })
    }

    pub fn get_default_commodity_id(&self) -> Option<CommodityId> {
        self.default_commodity_id
    }

    pub fn iter(&self) -> impl Iterator<Item = (&Commodity, CommodityId)> {
        self.commodities
            .iter()
            .enumerate()
            .map(|(idx, c)| (c, CommodityId::new(idx)))
    }

    pub fn lookup_id_by_name(&self, commodity: &str) -> Option<CommodityId> {
        self.commodities
            .iter()
            .position(|c| c.name == commodity)
            .map(|p| CommodityId::new(p))
    }

    pub fn lookup_name_by_id(&self, commodity_id: CommodityId) -> Option<&str> {
        self.commodities
            .get(commodity_id.index())
            .map(|c| c.name.as_str())
    }

    pub fn lookup_by_id(&self, commodity_id: CommodityId) -> Option<&Commodity> {
        self.commodities.get(commodity_id.index())
    }

    pub fn lookup_by_id_mut(&mut self, commodity_id: CommodityId) -> Option<&mut Commodity> {
        self.commodities.get_mut(commodity_id.index())
    }

    pub fn lookup_by_name_mut(&mut self, commodity: &str) -> Option<&mut Commodity> {
        self.commodities.iter_mut().find(|c| c.name == commodity)
    }

    pub fn get_or_add(&mut self, commodity: &str) -> Result<CommodityId, ()> {
        match self.lookup_id_by_name(commodity) {
            Some(id) => Ok(id),
            None => match self.strict_mode {
                CommodityStrictMode::Enabled => Err(()),
                CommodityStrictMode::Disabled { default_rounding } => {
                    let idx = CommodityId::new(self.commodities.len());
                    self.commodities.push(Commodity {
                        name: commodity.into(),
                        rounding: default_rounding,
                    });
                    Ok(idx)
                }
            },
        }
    }
}

#[test]
fn it_should_correctly_lookup_commodity_by_id() {
    let mut repo = CommodityRepository::new();
    let eur = repo.add("EUR").unwrap();
    let usd = repo.add("USD").unwrap();

    assert_eq!(Some("EUR"), repo.lookup_name_by_id(eur));
    assert_eq!(Some("USD"), repo.lookup_name_by_id(usd));
}

#[test]
fn it_should_correctly_lookup_commodity_by_name() {
    let mut repo = CommodityRepository::new();
    let eur = repo.add("EUR").unwrap();
    let usd = repo.add("USD").unwrap();

    assert_eq!(Some(eur), repo.lookup_id_by_name("EUR"));
    assert_eq!(Some(usd), repo.lookup_id_by_name("USD"));
    assert_eq!(None, repo.lookup_id_by_name("YEN"));
}
