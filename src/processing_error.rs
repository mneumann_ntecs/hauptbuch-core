#[derive(Debug)]
pub enum ProcessingError {
    TransactionNotBalanced,
    /// A transaction without any postings is not allowed
    TransactionEmpty,
    /// Only one valueless posting per transaction allowed
    AtMostOneValuelessPostingAllowed,
    /// Valueless posting must be of regular posting type
    ValuelessPostingOfNonRegularType,
    /// Value position with same commodity as main position detected
    ValuePositionWithSameCommodityAsMainPositionDetected,
    /// Balance within transaction must be of same commodities
    MultiValuedBalanceNotAllowed,

    ///
    UnitPriceAndTotalPriceMustHaveSameCommodity,
    UnitPriceAndTotalPriceInvalidCalculation,

    FatalOpenBalanceHasNoValue,
    CommodityMismatch,
    TransactionCommentOutsideTransaction,
    PostingOutsideTransaction,
    PostingCommentsUnimplemented,

    StrictAccountRegistry,
    StrictCommodityRegistry,
    NoDefaultCommoditySpecified,

    FatalAccountLookupFailure,
    FatalCommodityLookupFailure,

    /// Error when conversion from a string into a Decimal value fails.
    DecimalConversionError,
}
