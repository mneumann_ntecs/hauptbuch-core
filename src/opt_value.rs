use crate::CommodityId;
use crate::Value;
use rust_decimal::Decimal;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct OptValue {
    pub value: Option<Value>,
}

impl OptValue {
    pub fn new_with_value(value: Value) -> Self {
        Self { value: Some(value) }
    }

    pub fn from_opt_value(opt_value: Option<Value>) -> Self {
        Self { value: opt_value }
    }

    pub fn new() -> Self {
        Self { value: None }
    }

    pub fn amount_in_commodity(&self, commodity_id: CommodityId) -> Option<Decimal> {
        if let Some(v) = self.value {
            if v.commodity_id == commodity_id {
                return Some(v.amount);
            }
        }
        None
    }

    /// Transfers `value` to the optional value. `Value` has to be of the same commodity
    /// as the internally stored value.
    pub fn transfer(&mut self, value: Value) -> Result<(), ()> {
        match self.value {
            None => {
                self.value = Some(value);
                Ok(())
            }
            Some(prev_value) => {
                let new_value = prev_value.add(&value)?;
                self.value = Some(new_value);
                Ok(())
            }
        }
    }
}
