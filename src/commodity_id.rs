#[derive(Debug, PartialEq, Eq, Copy, Clone)]
pub struct CommodityId(usize);

impl CommodityId {
    pub(crate) fn index(&self) -> usize {
        self.0 as usize
    }

    pub(crate) fn new(index: usize) -> Self {
        Self(index)
    }
}
