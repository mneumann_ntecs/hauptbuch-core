use crate::CommodityId;
use rust_decimal::{prelude::Zero, Decimal};

/// A single commodity value.
#[derive(Debug, PartialEq, Eq, Copy, Clone)]
pub struct Value {
    pub commodity_id: CommodityId,
    pub amount: Decimal,
}

impl Value {
    pub fn has_same_commodity(&self, other: &Value) -> bool {
        self.commodity_id == other.commodity_id
    }

    pub fn zero(commodity_id: CommodityId) -> Self {
        Self {
            commodity_id,
            amount: Zero::zero(),
        }
    }
    pub fn is_zero(&self) -> bool {
        self.amount.is_zero()
    }

    pub fn negate(&self) -> Self {
        Self {
            amount: -self.amount,
            commodity_id: self.commodity_id,
        }
    }

    pub fn add(&self, other: &Value) -> Result<Self, ()> {
        if self.has_same_commodity(other) {
            Ok(Self {
                amount: self.amount + other.amount,
                commodity_id: self.commodity_id,
            })
        } else {
            Err(())
        }
    }

    pub fn transfer(&mut self, value: Value) -> Result<(), ()> {
        if self.has_same_commodity(&value) {
            self.amount = self.amount + value.amount;
            Ok(())
        } else {
            Err(())
        }
    }
}
